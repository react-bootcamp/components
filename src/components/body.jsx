import React from 'react';
import ReactDOM from 'react-dom';
import '../css/layout.css';

class Body extends React.Component { 
    render() {
        return( <div className="centerBody">
            <ul>
                <li >Name: {this.props.personalInfo.name} </li>
                <li>Age: {this.props.personalInfo.age}</li>
                <li>Address: {this.props.personalInfo.address}</li>
                <li>Occupation: {this.props.personalInfo.occupation}</li>
            </ul>
        </div>
        )
    }
}

export default Body;