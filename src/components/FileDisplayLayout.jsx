import React from 'react';
import ReactDOM from 'react-dom';
import Body from './body';
import Header from './header';
import Footer from './footer';

class FileDisplayLayout extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            personalInfo: {
                name: 'John Doe',
                age: '25',
                address: 'QC',
                occupation: 'Developer'
            }
        };
    }

    render() {
        return (
            <div>
                <Header />
                <hr />
                <Body personalInfo={this.state.personalInfo} />
                <hr />
                <Footer />
            </div>
        );
    }
}

export default FileDisplayLayout;